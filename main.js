// just for consistency i disabled the toggle ont the fullscreen button, just comment lin 395, and uncomment any call to // toggleFullScreenButton(); if you want it back
//
//
//
var shouldRedoRender = false;
var nameHelper = null;
var mouseMovementThreshold = 0;
var distanceBetween = -30;
//
//
// Mess around with these definitions to get better effects
//
// Some Browser Specific Definitions
document.requestPointerLock = document.requestPointerLock    ||
                              document.mozRequestPointerLock ||
                              document.webkitRequestPointerLock;

document.requestFullscreen = document.requestFullscreen    ||
                             document.mozrequestFullscreen ||
                             document.mozRequestFullScreen ||
                             document.webkitrequestFullscreen;
//
//
// PlanetTuple class for planets and sites
class PlanetTuple{
    siteName;
    siteUrl;
    planetFirstPos;
    planetLastPos;
    firstPosFlag;
    constructor(siteNameParam, siteUrlParam){
        this.siteName = siteNameParam;
        this.siteUrl = siteUrlParam;
        this.planetFirstPos = new  THREE.Vector3(0,0,0);
        this.planetLastPos = new THREE.Vector3(0,0,0);
        this.firstPosFlag = false;
    }
}
//
var camera, renderer, scene;
const margin = 10;
const clipping = 300;
const maxDistance = clipping*2;
const amount = 1000;
// array holding all the planets viewable at any point in time
var planets = new Array(24);
var colorsArrayWithWeights = [
    [[0,60],[100,100],[60,100],0.6],
    [[175,200],[100,100],[60,100],0.3],
    [[175,200],[100,100],[90,100],0.1]
];
// array holding all the textures
var maxTesturesNumber = 11;
var planetTexturesArray = [ // ---- 16;
'images/planet_1.png',
'images/planet_2.png',
'images/planet_3.png',
'images/planet_4.png',
'images/planet_5.png',
'images/planet_6.png',
'images/planet_7.png',
'images/planet_8.png',
'images/planet_9.png',
'images/planet_10.png',
'images/planet_11.png'
];
// list of websites in tuple type defined above
var maxListNumber = 10;
var siteList = new Array();
var sitenumber = 0;
// here is where the positions of the stars will be held
var starsMaterial,stars;
var starsPositionArray = new Float32Array(amount*3);
var starsGeometry = new THREE.BufferGeometry();
var colors = new Float32Array( amount * 3 );
var sizes = new Float32Array( amount );
var vertex = new THREE.Vector3();
var color = new THREE.Color( 0xffff00 );
var planetsCameraAlignedBoundingBoxGeometrySize = new THREE.Vector3(350, 340, 100)
var planetsCameraAlignedBoundingBoxGeometry = new THREE.BoxGeometry( 
    planetsCameraAlignedBoundingBoxGeometrySize.x,
    planetsCameraAlignedBoundingBoxGeometrySize.y,
    planetsCameraAlignedBoundingBoxGeometrySize.z
 );
var planetsCameraAlignedBoundingBoxMaterial = new THREE.MeshBasicMaterial( {
    color: 0x00ff00,
    transparent:true,
    opacity:0
} );
var planetsCameraAlignedBoundingBoxCubeMesh = new THREE.Mesh( planetsCameraAlignedBoundingBoxGeometry );
planetsCameraAlignedBoundingBoxCubeMesh.visible = false;
//
var raycaster, intersects;
var tabOpened = false;
// check 'F11' fullscreen action
var fullscreen = false;
//
const fontSize = 28
,     fontSizeMetrics = 'px'
,     font =  "bold sans-serif"
,     backGroundColor = "transparent", fontColor = "white"
,     fullscreenButton = document.getElementById("icon")
//
//
//
//
//
const inputKeys = {
    move:{
        forward:'w',
        backward:'s',
        left:'a',
        right:'d',
        upward:'r',
        downward:'f'
    },
    rotate:{
        left:'arrowleft',
        right:'arrowright',
        up:'arrowup',
        down:'arrowdown',
        rollLeft:'q',
        rollRight:'e'
    },
    run:' ',
    walk:'shift',
    click:'enter',
    f11Fullscreen:false
}
var inputVector = {
    forward:0,
    backward:0,
    left:0,
    right:0,
    upward:0,
    downward:0,
    rotateLeft:0,
    rotateRight:0,
    rotateUp:0,
    rotateDown:0,
    rollLeft:0,
    rollRight:0
}
var movementVectors = {
    translate:{
        x:0,
        y:0,
        z:0
    },
    rotate:{
        x:0,
        y:0,
        z:0
    }
};
var mouseInput = {
    x:0,
    y:0,
    sensibilityX:5,
    sensibilityY:5,
    rightClick:false,
    leftClick:false,
    isLocked:false
}
var raycastScreenPosition = new THREE.Vector2();
var speed = 1, delta = 0.5, rollSpeed = 0.02;
const planetsSize = 5;
const sceneBackground = new THREE.Color(0x040404);
const applicationPlacing = document.body;
var labelsPivots = new Array();
var attributes;
var starsTexture = new THREE.TextureLoader().load( "images/starWhite.png" );
// this function populates the list to be used
function populatePlanetSiteList(list, name="Change This Entire Function", constr){
    // there is the site Names and Urls
    console.log(name);
    const customSites = [
        "Google",
        "YouTube",
        "Facebook",
        "Wikipedia",
        "Yahoo!",
        "Amazon",
        "VKontakte",
        "Netflix",
        "Bing",
        "Instagram",
        "LinkedIn",
        "Twitter",
        "Ebay",
        "Twitch",
    "Dibujos"];
    const customSitesUrls = [
        "google.com",
        "youtube.com",
        "facebook.com",
        "wikipedia.org",
        "yahoo.com",
        "amazon.com",
        "vk.com",
        "netflix.com",
        "bing.com",
        "instagram.com",
        "linkedin.com",
        "twitter.com",
        "ebay.com",
        "twitch.tv",
        "pixiv.net"
    ];
    var csites2 = [
        "reddit",
        "medium",
        "stack overflow",
        "udacity"
    ];
    var csitesurl2 = [
        "reddit.com",
        "medium.com",
        "stackoverflow.com",
        "udacity.com"
    ];
    if(name === "Hello6"){    
        for(let i = 0; i < 4; i++){
            // populate the list calling the Tuple constructor defined above 
            // use to define a site with its position regarding to planets (this part is not yet implemented)
            // and its name and url
            list.push(new constr(csites2[i],csitesurl2[i]));
        }
    }else{    
        for(let i = 0; i < 14; i++){
            // populate the list calling the Tuple constructor defined above 
            // use to define a site with its position regarding to planets (this part is not yet implemented)
            // and its name and url
            list.push(new constr(customSites[i],customSitesUrls[i]));
        }
    } 
}
function setSkybox(sceneParam){
    sceneParam.background = new THREE.CubeTextureLoader()
	.setPath( 'images/' )
	.load( [
		'skybox_rt2.png',
		'skybox_lt2.png',
		'skybox_up2.png',
		'skybox_dn2.png',
		'skybox_ft2.png',
		'skybox_bk2.png'
	] );
}
function createPlanet(position = new THREE.Vector3(), textStr,index){
    // Model with Mat
    var planetGeometry = new THREE.SphereGeometry(planetsSize,24,12);
    // Load a random texture 
    var randomTextureImageName = planetTexturesArray[getRandomInt(0, planetTexturesArray.length)];//planetTexturesArray[getRandomInt(0, planetTexturesArray.length)];
    var planetTexture = new THREE.TextureLoader().load(randomTextureImageName); // randomTexture
    var PlanetMaterial = new THREE.MeshBasicMaterial({ map: planetTexture,});
    var planetMesh = new THREE.Mesh( planetGeometry, PlanetMaterial );
    //
    var label = makeLabel(100,font,fontSize,fontSizeMetrics,textStr);
    var textTexture = new THREE.CanvasTexture(label.canvas);
    // because our canvas is likely not a power of 2
    // in both dimensions set the filtering appropriately.
    textTexture.minFilter = THREE.LinearFilter;
    textTexture.wrapS = THREE.ClampToEdgeWrapping;
    textTexture.wrapT = THREE.ClampToEdgeWrapping;
    var textMaterial = new THREE.SpriteMaterial({
        map: textTexture,
        transparent: true,
        side:THREE.DoubleSide
      });
    var planetGroup = new THREE.Group();
    planetGroup.position.x = 100;
    var textObject = new THREE.Sprite(textMaterial);
    // set text position
    // if units are meters then 0.01 here makes size
    // of the label into centimeters.
    var labelBaseScale = 0.01*planetsSize;
    textObject.scale.x = label.canvas.width*labelBaseScale;
    textObject.scale.y = label.canvas.height*labelBaseScale;

    var pivot = new THREE.Object3D();
    pivot.add(textObject);
    textObject.position.y = (planetMesh.position.y + planetsSize*1.5 );
    //
    planetGroup.add(planetMesh);
    planetGroup.add(pivot);
    //
    planetMesh.position.set(0,0,0);
    pivot.position.set(0,0,0);
    //
    planetMesh.name = textStr;
    textObject.name = textStr;
    scene.add(planetGroup);
    planetGroup.translateX(position.x);
    planetGroup.translateY(position.y);
    planetGroup.translateZ(position.z);
    //
    labelsPivots[index] = pivot;
    //
    return planetGroup;
}

function randomPlanetMove(){
    const minValues = new THREE.Vector3(
        (planetsCameraAlignedBoundingBoxGeometrySize.x/2)*-1,
        (planetsCameraAlignedBoundingBoxGeometrySize.y/2)*-1,
        (planetsCameraAlignedBoundingBoxGeometrySize.z/2)*-1
    );
    const maxValues = new THREE.Vector3(
        (planetsCameraAlignedBoundingBoxGeometrySize.x/2),
        (planetsCameraAlignedBoundingBoxGeometrySize.y/2),
        (planetsCameraAlignedBoundingBoxGeometrySize.z/2)
    );
    const randomVector = new THREE.Vector3(
        getRandomArbitrary(minValues.x, maxValues.x),
        getRandomArbitrary(minValues.y, maxValues.y),
        getRandomArbitrary(minValues.z, maxValues.z)
    );
    var offset = new THREE.Vector3(
        planetsCameraAlignedBoundingBoxCubeMesh.position.x,
        planetsCameraAlignedBoundingBoxCubeMesh.position.y,
        planetsCameraAlignedBoundingBoxCubeMesh.position.z
    );
    camera.localToWorld(offset);
    var returnVector = new THREE.Vector3();
    returnVector.x = randomVector.x + offset.x;
    returnVector.y = randomVector.y + offset.y;
    returnVector.z = randomVector.z + offset.z;
    //
    //
    return returnVector;
    //
}

function makeLabel(baseWidth, font, fontSize, fontSizeMetrics, textStr){
    //
    const borderSize = 2;
    const canvasForText=document.createElement('canvas');
    const ctx = canvasForText.getContext('2d');
    ctx.font = fontSize.toString()+fontSizeMetrics+' '+font;
    ctx.font = parseFloat(getComputedStyle(ctx.canvas).fontSize).toString()+'px '+font;
    // measure how long the name will be
    const textWidth = ctx.measureText(textStr).width;

    const doubleBorderSize = borderSize * 2;
    const width = ctx.measureText(textStr).width + doubleBorderSize;
    const height = fontSize + doubleBorderSize*3;
    ctx.canvas.width = width;
    ctx.canvas.height = height;

    // need to set font again after resizing canvas
    ctx.font = fontSize.toString()+fontSizeMetrics+' '+font;
    ctx.font = parseFloat(getComputedStyle(ctx.canvas).fontSize).toString()+'px '+font;
    ctx.textBaseline = 'top';
    ctx.textAlign = 'center';

    ctx.fillStyle = backGroundColor;
    ctx.fillRect(0, 0, width, height);

    // scale to fit but don't stretch
    const scaleFactor = Math.min(1, baseWidth / textWidth);
    ctx.translate(width / 2, height / 3);
    ctx.scale(scaleFactor, 1);
    ctx.fillStyle = fontColor;
    ctx.fillText(textStr, 0, 0);

    return {canvas:ctx.canvas, width:textWidth};
}

function updateMovementVector(){
	movementVectors.translate.x = ( - inputVector.left + inputVector.right);
	movementVectors.translate.y = ( - inputVector.downward + inputVector.upward );
	movementVectors.translate.z = ( - inputVector.forward + inputVector.backward);
}

// Handle Key presses
function keyboardButtonDownEventHandler(event){
    if(event.key){
        switch(event.key.toLowerCase()){
            case inputKeys.move.forward:
                inputVector.forward = 1;
                break;
            case inputKeys.move.backward:
                inputVector.backward = 1;
                break;
            case inputKeys.move.left:
                inputVector.left = 1;
                break;
            case inputKeys.move.right:
                inputVector.right = 1;
                break;
            case inputKeys.move.upward:
                inputVector.upward = 1;
                break;
            case inputKeys.move.downward:
                inputVector.downward = 1;
                break;
            case inputKeys.rotate.left:
                inputVector.rotateLeft = 1;
                break;
            case inputKeys.rotate.right:
                inputVector.rotateRight = 1;
                break;
            case inputKeys.rotate.up:
                inputVector.rotateUp = 1;
                break;
            case inputKeys.rotate.down:
                inputVector.rotateDown = 1;
                break;
            case inputKeys.rotate.rollLeft:
                inputVector.rollLeft = 1;
                break;
            case inputKeys.rotate.rollRight:
                inputVector.rollRight = 1;
                break;
            case 'f11':
                fullscreen = true;
                // toggleFullScreenButton();
                setTimeout(function(){ inputKeys.f11Fullscreen = true; }, 150); // ---- This Line
                document.documentElement.requestPointerLock();
                setTimeout(function(){tabOpened = false;},250);
                break;
            case inputKeys.click:
                raycaster.setFromCamera( new THREE.Vector2(), camera );
                // calculate objects intersecting the picking ray
                intersects = raycaster.intersectObjects(scene.children, true);
                //
                if(intersects != undefined && intersects.length > 0 && intersects[0] != undefined){
                    if(intersects[0].object.parent != undefined && intersects[0].object.geometry instanceof THREE.SphereGeometry && !tabOpened){
                        openNewTab(getValueByName(intersects[0].object.name));
                    }
                }
                break;
                
        }
        //
        if(event.key.toLowerCase() === inputKeys.walk){
            speed = 0.2;
        }else if(event.key.toLowerCase() === inputKeys.run){
            speed = 2;
        }
    }
    updateMovementVector();
}
// Handle the release of keys
function keyboardButtonUpEventHandler(event){
    if(event.key){
        switch(event.key.toLowerCase()){
            case inputKeys.move.forward:
                inputVector.forward = 0;
                break;
            case inputKeys.move.backward:
                inputVector.backward = 0;
                break;
            case inputKeys.move.left:
                inputVector.left = 0;
                break;
            case inputKeys.move.right:
                inputVector.right = 0;
                break;
            case inputKeys.move.upward:
                inputVector.upward = 0;
                break;
            case inputKeys.move.downward:
                inputVector.downward = 0;
                break;
            case inputKeys.rotate.left:
                inputVector.rotateLeft = 0;
                break;
            case inputKeys.rotate.right:
                inputVector.rotateRight = 0;
                break;
            case inputKeys.rotate.up:
                inputVector.rotateUp = 0;
                break;
            case inputKeys.rotate.down:
                inputVector.rotateDown = 0;
                break;
            case inputKeys.rotate.rollLeft:
                inputVector.rollLeft = 0;
                break;
            case inputKeys.rotate.rollRight:
                inputVector.rollRight = 0;
                break;
        }
        //
        if(event.key.toLowerCase() === inputKeys.walk){
            speed = 1;
        }else if(event.key.toLowerCase() === inputKeys.run){
            speed = 1;
        }
    }
    updateMovementVector();
}

// Where should we move?
function updateMovement(cam){
    var moveMult = delta * speed;

    cam.translateX( movementVectors.translate.x * moveMult );
    cam.translateY( movementVectors.translate.y * moveMult );
    cam.translateZ( movementVectors.translate.z * moveMult );
    //
}

function updateRotation(cam){
    //
    var rollSpeedMult = delta * (rollSpeed * speed);
    var rotMultX = delta * (mouseInput.sensibilityX*0.001);
    var rotMultY = delta * (mouseInput.sensibilityY*0.001);
    //
    if(mouseInput.y != 0 && mouseInput.rightClick){
        const moveY = mouseInput.y > mouseMovementThreshold ? mouseInput.y: mouseInput.y < -mouseMovementThreshold ? mouseInput.y: 0; 
        movementVectors.rotate.x = - moveY* rotMultY;
    }
    if(mouseInput.x != 0 && mouseInput.rightClick){
        const moveX = mouseInput.x > mouseMovementThreshold ? mouseInput.x: mouseInput.x < -mouseMovementThreshold ? mouseInput.x:0; 
        movementVectors.rotate.y = - moveX* rotMultX;
    }
    //
    if(mouseInput.y != 0 && mouseInput.isLocked){
        const moveY = mouseInput.y > mouseMovementThreshold ? mouseInput.y: mouseInput.y < -mouseMovementThreshold ? mouseInput.y: 0; 
        movementVectors.rotate.x = - moveY* rotMultY;
    }
    if(mouseInput.x != 0 && mouseInput.isLocked){
        const moveX = mouseInput.x > mouseMovementThreshold ? mouseInput.x: mouseInput.x < -mouseMovementThreshold ? mouseInput.x:0; 
        movementVectors.rotate.y = - moveX* rotMultX;
    }
    //
    if(inputVector.rotateDown || inputVector.rotateUp){
        movementVectors.rotate.x = ( - inputVector.rotateDown + inputVector.rotateUp ) * rollSpeedMult;
    }
    if(inputVector.rotateLeft || inputVector.rotateRight){
        movementVectors.rotate.y = ( - inputVector.rotateRight + inputVector.rotateLeft) * rollSpeedMult;
    }
    if(inputVector.rollLeft || inputVector.rollRight){
        movementVectors.rotate.z = ( - inputVector.rollRight + inputVector.rollLeft) * rollSpeedMult;
    }
    cam.rotateX(movementVectors.rotate.x); 
    cam.rotateY(movementVectors.rotate.y); 
    cam.rotateZ(movementVectors.rotate.z);
    movementVectors.rotate.x=0;
    movementVectors.rotate.y=0;
    movementVectors.rotate.z=0;
    //
}

function mouseDblclick(container){
    document.documentElement.requestPointerLock();
    setTimeout(function(){ tabOpened = false; }, 250);
    mouseInput.isLocked = true;
}

function defineControls(container){
    document.addEventListener('keydown',keyboardButtonDownEventHandler,false);
    document.addEventListener('keyup',keyboardButtonUpEventHandler,false);
    document.addEventListener('dblclick',function(event){mouseDblclick(container);},false);
    document.addEventListener('mousedown',mouseCameraClick,false);
    document.addEventListener('mouseup',function(e){mouseClick(e);},false);
    document.addEventListener('mousemove',function(event){mouseMoveEventHandler(event,camera,container);},false);
    document.addEventListener('contextmenu',function(event){ event.preventDefault();},false);
    document.addEventListener('blur', windowLoseFocus, false);
    //
    window.addEventListener('resize', function(event){checkF11Fullscreen(event)} , false);
    window.addEventListener('beforeunload', function(event){
        if(!tabOpened){
            event.preventDefault();
            // Chrome requires returnValue to be set.
            event.returnValue = '';
        }
      });
}

function movePlanets(i,planetNewPos){
    if(sitenumber < maxListNumber && (siteList[sitenumber] != null || siteList[sitenumber] != undefined )){
        var planetInstancePosition = planetNewPos;
        // HERE
        if(!siteList[sitenumber].firstPosFlag){
            siteList[sitenumber].planetFirstPos.set(
                planetInstancePosition.x,
                planetInstancePosition.y,
                planetInstancePosition.z
            );
            siteList[sitenumber].firstPosFlag = true;
            planets[i].position.set(
                planetInstancePosition.x,
                planetInstancePosition.y,
                planetInstancePosition.z
            );
            planets[i].name = siteList[sitenumber].siteName;
        }else if(siteList[sitenumber].firstPosFlag){
            siteList[sitenumber].planetLastPos.set(
                planetInstancePosition.x,
                planetInstancePosition.y,
                planetInstancePosition.z
            );
            siteList[sitenumber].firstPosFlag = false;
            planets[i].position.set(
                planetInstancePosition.x,
                planetInstancePosition.y,
                planetInstancePosition.z
            );
            planets[i].name = siteList[sitenumber].siteName;
        }
    }else{
        sitenumber = 0;
        var planetInstancePosition = planetNewPos;
        if(!siteList[sitenumber].firstPosFlag){
            siteList[sitenumber].planetFirstPos.set(
                planetInstancePosition.x,
                planetInstancePosition.y,
                planetInstancePosition.z
            );
            siteList[sitenumber].firstPosFlag = true;
            planets[i].position.set(
                planetInstancePosition.x,
                planetInstancePosition.y,
                planetInstancePosition.z
            );
            planets[i].name = siteList[sitenumber].siteName;
        }else if(siteList[sitenumber].firstPosFlag){
            siteList[sitenumber].planetLastPos.set(
                planetInstancePosition.x,
                planetInstancePosition.y,
                planetInstancePosition.z
            );
            siteList[sitenumber].firstPosFlag = false;
            planets[i].position.set(
                planetInstancePosition.x,
                planetInstancePosition.y,
                planetInstancePosition.z
            );
            planets[i].name = siteList[sitenumber].siteName;
        }
    }
}

function moveStars(cameraPosition){
    for ( var i = 0; i < starsPositionArray.length; i +=3) {
        const pos = new THREE.Vector3(starsPositionArray[i],starsPositionArray[i+1],starsPositionArray[i+2]);
        const trans = stars.localToWorld(pos);
        if( cameraPosition.distanceTo(trans) > maxDistance+margin){
            //
            var tst = methodPoint(maxDistance-clipping, clipping+margin);
            tst.x = (tst.x) + cameraPosition.x;
            tst.y = (tst.y) + cameraPosition.y;
            tst.z = (tst.z) + cameraPosition.z;
            stars.worldToLocal(tst);
            var radomStarColorVariable = getRandomStarColor();
            color = new THREE.Color(radomStarColorVariable[0],radomStarColorVariable[1],radomStarColorVariable[2]);
            color.toArray(colors, i);
            starsPositionArray[ i   ] = tst.x;
            starsPositionArray[ i+1 ] = tst.y;
            starsPositionArray[ i+2 ] = tst.z;
        }
    }
    starsGeometry.setAttribute( 'customColor', new THREE.BufferAttribute( colors, 3 ) );
}
function moveStarCenter(cameraPosition){
    for ( var i = 0; i < starsPositionArray.length; i +=3) {
        const pos = new THREE.Vector3(starsPositionArray[i],starsPositionArray[i+1],starsPositionArray[i+2]);
        pos.x = (pos.x+stars.position.x) - cameraPosition.x;
        pos.y = (pos.y+stars.position.y) - cameraPosition.y;
        pos.z = (pos.z+stars.position.z) - cameraPosition.z;
        //
        starsPositionArray[ i   ] = pos.x;
        starsPositionArray[ i+1 ] = pos.y;
        starsPositionArray[ i+2 ] = pos.z;
    }
}

function init(){
    //Anything UI
    setCategoriesList();
    //
    //
    //
    //
    populatePlanetSiteList(siteList, nameHelper,PlanetTuple.prototype.constructor);
    scene = new THREE.Scene();
    scene.background = sceneBackground;
    camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, clipping );
    scene.add(camera);
    camera.position.set(0,0,0);
    camera.add(planetsCameraAlignedBoundingBoxCubeMesh);
    planetsCameraAlignedBoundingBoxCubeMesh.translateZ(-300);
    renderer = new THREE.WebGLRenderer();
    renderer.setSize( window.innerWidth, window.innerHeight );
    applicationPlacing.appendChild( renderer.domElement );
    raycaster = new THREE.Raycaster();
    
    defineControls(renderer.domElement);
    setSkybox(scene);
    for(var i = 0; i < planets.length; i++, sitenumber++){
        if(siteList[sitenumber]){
            var planetInstancePosition = randomPlanetposition(margin*5, clipping/2,camera.position);
            for(let j = 0; j < planets.length;j++){
                if(planets[j] && planets[j] != planets[i] ){
                    if(planets[j].position.distanceTo(planetInstancePosition) < 30){
                        var distance = new THREE.Vector3(distanceBetween,distanceBetween,distanceBetween);
                        planetInstancePosition.add(distance);
                    }
                }
            }
            if(sitenumber < maxListNumber && (siteList[sitenumber] != null || siteList[sitenumber] != undefined )){
                if(!siteList[sitenumber].firstPosFlag){
                    planets[i] = createPlanet(planetInstancePosition,siteList[sitenumber].siteName,i);
                    siteList[sitenumber].planetFirstPos.set(
                        planetInstancePosition.x,
                        planetInstancePosition.y,
                        planetInstancePosition.z
                    );
                    siteList[sitenumber].firstPosFlag = true;
                }else if(siteList[sitenumber].firstPosFlag){
                    planets[i] = createPlanet(planetInstancePosition,siteList[sitenumber].siteName,i);
                    siteList[sitenumber].planetLastPos.set(
                        planetInstancePosition.x,
                        planetInstancePosition.y,
                        planetInstancePosition.z
                    );
                    siteList[sitenumber].firstPosFlag = false;
                }
            }else{
                if(!siteList[sitenumber].firstPosFlag){
                    planets[i] = createPlanet(planetInstancePosition,siteList[getRandomInt(0,siteList.length-1)].siteName,i);
                    siteList[sitenumber].planetFirstPos.set(
                        planetInstancePosition.x,
                        planetInstancePosition.y,
                        planetInstancePosition.z
                    );
                    siteList[sitenumber].firstPosFlag = true;
                }else if(siteList[sitenumber].firstPosFlag){
                    planets[i] = createPlanet(planetInstancePosition,siteList[getRandomInt(0,siteList.length-1)].siteName,i);
                    siteList[sitenumber].planetLastPos.set(
                        planetInstancePosition.x,
                        planetInstancePosition.y,
                        planetInstancePosition.z
                    );
                    siteList[sitenumber].firstPosFlag = false;
                }
            }
        }else{
            var planetInstancePosition = randomPlanetposition(margin*5, clipping/2,camera.position);
            for(let j = 0; j < planets.length;j++){
                if(planets[j] && planets[j] != planets[i] ){
                    if(planets[j].position.distanceTo(planetInstancePosition) < 30){
                        var distance = new THREE.Vector3(distanceBetween,distanceBetween,distanceBetween);
                        planetInstancePosition.add(distance);
                    }
                }
            }
            customNumber = getRandomInt(0,siteList.length-1);
            if(customNumber < maxListNumber && (siteList[customNumber] != null || siteList[customNumber] != undefined )){
                if(!siteList[customNumber].firstPosFlag){
                    planets[i] = createPlanet(planetInstancePosition,siteList[customNumber].siteName,i);
                    siteList[customNumber].planetFirstPos.set(
                        planetInstancePosition.x,
                        planetInstancePosition.y,
                        planetInstancePosition.z
                    );
                    siteList[customNumber].firstPosFlag = true;
                }else if(siteList[customNumber].firstPosFlag){
                    planets[i] = createPlanet(planetInstancePosition,siteList[customNumber].siteName,i);
                    siteList[customNumber].planetLastPos.set(
                        planetInstancePosition.x,
                        planetInstancePosition.y,
                        planetInstancePosition.z
                    );
                    siteList[customNumber].firstPosFlag = false;
                }
            }else{
                if(!siteList[customNumber].firstPosFlag){
                    planets[i] = createPlanet(planetInstancePosition,siteList[getRandomInt(0,siteList.length-1)].siteName,i);
                    siteList[customNumber].planetFirstPos.set(
                        planetInstancePosition.x,
                        planetInstancePosition.y,
                        planetInstancePosition.z
                    );
                    siteList[customNumber].firstPosFlag = true;
                }else if(siteList[customNumber].firstPosFlag){
                    planets[i] = createPlanet(planetInstancePosition,siteList[getRandomInt(0,siteList.length-1)].siteName,i);
                    siteList[customNumber].planetLastPos.set(
                        planetInstancePosition.x,
                        planetInstancePosition.y,
                        planetInstancePosition.z
                    );
                    siteList[customNumber].firstPosFlag = false;
                }
            }
        }
    }
    for ( var i = 0; i < amount; i ++ ) {
        const tst = methodPoint(maxDistance-margin, margin*5);
        //
        vertex.x = camera.position.x + tst.x;
        vertex.y = camera.position.y + tst.y;
        vertex.z = camera.position.z + tst.z;
        vertex.toArray( starsPositionArray, i * 3 );
        //
        var radomStarColorVariable = getRandomStarColor();
        color = new THREE.Color(radomStarColorVariable[0],radomStarColorVariable[1],radomStarColorVariable[2]);
        color.toArray( colors, i * 3 );
        sizes[ i ] = 5;
    }
    var bufferedPositions = new THREE.BufferAttribute( starsPositionArray, 3 );
    starsGeometry.setAttribute( 'position',  bufferedPositions);
    starsTexture.wrapS = THREE.RepeatWrapping;
    starsTexture.wrapT = THREE.RepeatWrapping;
    
    starsMaterial = new THREE.PointsMaterial( { 
        size: 3, 
        sizeAttenuation: true, 
        map: starsTexture, 
        alphaTest: 0.5, 
        transparent: false,
        side: THREE.DoubleSide,
        depthTest: true,
    } );
    stars = new THREE.Points( starsGeometry, starsMaterial );
    scene.add( stars );
    if(fullscreenButton){
        fullscreenButton.addEventListener("click",function(){
            document.documentElement.requestFullscreen();
            var testValue = isDocumentInFullScreenMode();
            if(testValue === undefined){
                setTimeout(function(){
                    document.documentElement.requestPointerLock();
                    mouseInput.isLocked = true;
                },150);
            }
            tabOpened = false;
        },false);
    }
    // ---------------------------------------------
    animate();
    //
}

function animate(){
    if(sitenumber >= maxListNumber){
        sitenumber = 0;
    }
    //
    updateMovement(camera);
    updateRotation(camera);
    //
    //
    for(var j = 0; j < labelsPivots.length; j++){
        if(labelsPivots[j]){
            labelsPivots[j].lookAt(camera.position);
        }
    }
    renderer.render( scene, camera );
    
    if(!shouldRedoRender){
        requestAnimationFrame( animate );
    }else{
        redoRender(nameHelper);
    }
}

// Helper function to check for fullscreen mode using the FullScreenElement as base (PD: does not work with 'F11' fullscreen)
function isDocumentInFullScreenMode() {
    const doc = document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement || document.msFullscreenElement;
    return doc;
}
// Shows or hides the FullScreen Button
function toggleFullScreenButton(){
    if(!isDocumentInFullScreenMode()){
        if(fullscreen){
            fullscreenButton.style.display = "none";
            setTimeout(function(){ inputKeys.f11Fullscreen = true; }, 150);
        }else{
            fullscreenButton.style.display = "block";
        }
    }else{
        fullscreenButton.style.display = "none";
    }
}
// check 'F11' fullscreen action
// also is a resize function so use it for everything resize
function checkF11Fullscreen(){
    resizeRendererToDisplaySize(renderer);
    camera.aspect = window.innerWidth /window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth,window.innerHeight);
    placeCrossHair();
    setCategoriesList();
    if(!isDocumentInFullScreenMode() && inputKeys.f11Fullscreen && fullscreen) {
        inputKeys.f11Fullscreen = false;
        fullscreen = false;
        applicationPlacing.focus();
        // toggleFullScreenButton();
        document.exitPointerLock();
    }
    //
}

function pointerLockChange() {
    document.pointerLockElement = document.pointerLockElement    ||
                                  document.mozPointerLockElement ||
                                  document.webkitPointerLockElement;
  
    if (!!document.pointerLockElement) {
        mouseInput.isLocked = true;
    } else {
        mouseInput.isLocked = false;
    }
}

function methodPoint(RADIUS, voidRadius) {
    var u = Math.random();
    var x1 = randn();
    var x2 = randn();
    var x3 = randn();
    var c = Math.cbrt(u) / Math.sqrt(x1 * x1 + x2 * x2 + x3 * x3);
    c *= RADIUS;
    return new THREE.Vector3(
        (x1) * 
        (c > 0 ? c + voidRadius : c < 0 ? c - voidRadius:0),
        (x2) * 
        (c > 0 ? c + voidRadius : c < 0 ? c - voidRadius:0),
        (x3) * 
        (c > 0 ? c + voidRadius : c < 0 ? c - voidRadius:0)
        );
};

function randn() {
    var u = 0
      , v = 0;
    while (u === 0)
        u = Math.random();
    while (v === 0)
        v = Math.random();
    return Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v);
}

function openNewTab(url){
    window.open(url);
}

// get url from List 
function getValueByName(name){
    var place = 0;
    for(let i = 0; i < siteList.length; i++){
        if(siteList[i].siteName === name){
            place = i;
            break;
        }
    }
    return "https://www."+siteList[place].siteUrl;
}

function mouseClick(mouseUpEvent){
    raycastScreenPosition.x = ( mouseUpEvent.clientX / window.innerWidth ) * 2 - 1;
    raycastScreenPosition.y = - ( mouseUpEvent.clientY / window.innerHeight ) * 2 + 1;
	mouseUpEvent.preventDefault();
    //
    const rightClick = mouseUpEvent.buttons & 2;
    const leftClick = mouseUpEvent.buttons & 1;
    if(rightClick === 0){
        movementVectors.rotate.x = 0;
        movementVectors.rotate.y = 0;
        mouseInput.rightClick = false;
    }
    if(leftClick === 0 && mouseInput.leftClick){
        // update the picking ray with the camera and mouse position
        if(mouseInput.leftClick){
            if(mouseInput.isLocked){
                raycaster.setFromCamera( new THREE.Vector2(), camera );
            }else{
                raycaster.setFromCamera( raycastScreenPosition, camera );
            }
            // calculate objects intersecting the picking ray
            intersects = raycaster.intersectObjects(scene.children, true);
            //
            //
            if(intersects != undefined && intersects.length > 0 && intersects[0] != undefined){
                if(intersects[0].object.parent != undefined && intersects[0].object.geometry instanceof THREE.SphereGeometry && !tabOpened){
                    tabOpened = true;
                    openNewTab(getValueByName(intersects[0].object.name));
                }
            }
            mouseInput.leftClick = false;
        }
    }
}

function mouseCameraClick(mouseDownEvent){
    const leftClick = mouseDownEvent.buttons & 1;
    const rightClick = mouseDownEvent.buttons & 2;
    if(rightClick === 2){
        mouseInput.rightClick = true;
        //
        var logList = [];
        for(let i = 0;i < planets.length;i++){
            logList.push(planets[i].position);
        }
        // console.log(logList);
    }
    if(leftClick === 1){
        mouseInput.leftClick = true;
    }
    setTimeout(function(){
        if(tabOpened){
            tabOpened = false
        }
    }, 350);
}

function mouseMoveEventHandler(mouseMoveEvent){
    mouseInput.y  = mouseMoveEvent.movementY || mouseMoveEvent.mozMovementY || mouseMoveEvent.webkitMovementY || 0;
    mouseInput.x = mouseMoveEvent.movementX || mouseMoveEvent.mozMovementX || mouseMoveEvent.webkitMovementX || 0;
}

// CHECK
function windowLoseFocus(){
    inputVector = {
        forward:0,
        backward:0,
        left:0,
        right:0,
        upward:0,
        downward:0,
        rotateLeft:0,
        rotateRight:0,
        rotateUp:0,
        rotateDown:0,
        rollLeft:0,
        rollRight:0
    }
    mouseInput.x = 0;
    mouseInput.y = 0;
    mouseInput.isLocked = false;
    mouseInput.leftClick = false;
    mouseInput.rightClick = false;
    movementVectors.translate.x = 0;
    movementVectors.translate.y = 0;
    movementVectors.translate.z = 0;
    movementVectors.rotate.x = 0;
    movementVectors.rotate.y = 0;
    movementVectors.rotate.z = 0;
    colorUnpressingBlur();
}

if(document.addEventListener){
    // document.addEventListener('fullscreenchange', toggleFullScreenButton, false);
    // document.addEventListener('mozfullscreenchange', toggleFullScreenButton, false);
    // document.addEventListener('MSfullscreenchange', toggleFullScreenButton, false);
    // document.addEventListener('webkitfullscreenchange', toggleFullScreenButton, false);
    // 
    document.addEventListener('pointerlockchange', pointerLockChange, false);
    document.addEventListener('mozpointerlockchange', pointerLockChange, false);
    document.addEventListener('webkitpointerlockchange', pointerLockChange, false);
}
//
//
// CHECK
function resizeRendererToDisplaySize(renderer) {
  const canvas = renderer.domElement;
  const width = canvas.clientWidth;
  const height = canvas.clientHeight;
  const needResize = canvas.width !== width || canvas.height !== height;
  if (needResize) {
    renderer.setSize(width, height, false);
  }
  return needResize;
}

init();
//updateuniverse
var updateStarsTimer = setInterval(function(){
    if(sitenumber >= maxListNumber){
        sitenumber = 0;
    }
    resizeRendererToDisplaySize(renderer);
    starsGeometry = stars.geometry;
    const cameraPosition = new THREE.Vector3(camera.position.x,camera.position.y,camera.position.z);
    //
    moveStars(cameraPosition);
    //
    if(camera.position.distanceTo(stars.position) > maxDistance+margin){
        moveStarCenter(cameraPosition);
        stars.position.set(cameraPosition.x,cameraPosition.y,cameraPosition.z);
    }
    for(var i = 0; i < planets.length; i++,sitenumber++){
        if(sitenumber > maxListNumber){
            sitenumber = 0;
        }
        if(camera.position.distanceTo(planets[i].position) > maxDistance*0.7){
            const newPos = randomPlanetMove();
            for(let j = 0; j < planets.length;j++){
                if(planets[j].position.distanceTo(newPos) < 30 && planets[j] != planets[i]){
                    var distance = new THREE.Vector3(distanceBetween,distanceBetween,distanceBetween);
                    newPos.add(distance);
                }
            }
            movePlanets(i, newPos);
        }
    }
    starsGeometry.attributes.position.needsUpdate = true;
    starsGeometry.computeBoundingSphere();
}, 105);
// excluding hue ranges
// 60 - 175
// 175 - 200
// saturation 100%
// including light range
// 60 - 100
function normalizeValues(value, max, min) { return (value - min) / (max - min); }
function randomWithExclusion(max, exclude) {
    let excluded_range = exclude[1] - exclude[0]
    let randonNumber = Math.random() * (max - excluded_range)
    if (randonNumber > exclude[0]) rand += excluded_range
    return randonNumber
}
function hslToRgb(hue, sat, light){
    var h,s,l;
    var r, g, b;
    h = normalizeValues(hue,360,0);
    s = normalizeValues(sat,100,0);
    l = normalizeValues(light,100,0);

    if(s == 0){
        r = g = b = l; // achromatic
    }else{
        var hue2rgb = function hue2rgb(p, q, t){
            if(t < 0) t += 1;
            if(t > 1) t -= 1;
            if(t < 1/6) return p + (q - p) * 6 * t;
            if(t < 1/2) return q;
            if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
            return p;
        }

        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        var p = 2 * l - q;
        r = hue2rgb(p, q, h + 1/3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1/3);
    }

    return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
}
function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

///Star and Nebula Random color with probablity
function getRandomStarColor(){
    var total_weight = 0;
    for(var i = 0; i < colorsArrayWithWeights.length;i++){
        total_weight += colorsArrayWithWeights[i][3];
    }
    var random_num = getRandomArbitrary(0, total_weight);
    var weight_sum = 0;
     
    for (var i = 0; i < colorsArrayWithWeights.length; i++) {
        weight_sum += colorsArrayWithWeights[i][3];
        weight_sum = +weight_sum.toFixed(2);
         
        if (random_num <= weight_sum) {
            var randomColorReturnValue = hslToRgb(
                getRandomArbitrary(colorsArrayWithWeights[i][0][0],colorsArrayWithWeights[i][0][1]),
                getRandomArbitrary(colorsArrayWithWeights[i][1][0],colorsArrayWithWeights[i][1][1]),
                getRandomArbitrary(colorsArrayWithWeights[i][2][0],colorsArrayWithWeights[i][2][1]),
            );
            return randomColorReturnValue;
        }
    }
}

function randomPlanetposition(min, max,cameraPosition){
    //
    var positionVector = new THREE.Vector3();
    const tst = methodPoint(clipping*0.5, clipping*0.3);
    //
    positionVector.x = camera.position.x + tst.x;
    positionVector.y = camera.position.y + tst.y;
    positionVector.z = camera.position.z + tst.z;
    //
    return positionVector;
}
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}
///TODO
///Nebulae

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
function redoRenderHelper(name){
    shouldRedoRender = true;
    nameHelper = name;
}
function redoRender(name){
    siteList = new Array(0);
    populatePlanetSiteList(siteList, name,PlanetTuple.prototype.constructor);
    sitenumber = 0;
    for(var i = 0; i < planets.length; i++, sitenumber++){
        var planetInstancePosition = new THREE.Vector3(planets[i].position.x,planets[i].position.y,planets[i].position.z);
        if(i > 0){
            for(let j = 0; j < planets.length; j++){
                if(planets[j] && planets[j] != planets[i] ){    
                    if(planets[j].position.distanceTo(planetInstancePosition) < 30){
                        var distance = new THREE.Vector3(distanceBetween,distanceBetween,distanceBetween);
                        planetInstancePosition.add(distance);
                    }
                }
            }
        }
        if(siteList[sitenumber]){
            if(sitenumber < maxListNumber && (siteList[sitenumber] != null || siteList[sitenumber] != undefined )){
                if(!siteList[sitenumber].firstPosFlag){
                    siteList[sitenumber].planetFirstPos.set(
                        planetInstancePosition.x,
                        planetInstancePosition.y,
                        planetInstancePosition.z
                    );
                    siteList[sitenumber].firstPosFlag = true;
                    scene.remove(planets[i]);
                    planets[i].remove(labelsPivots[i]);
                    labelsPivots[i] = null
                    planets[i] = null;
                    planets[i] = createPlanet(planetInstancePosition,siteList[sitenumber].siteName,i);
                }else if(siteList[sitenumber].firstPosFlag){
                    siteList[sitenumber].planetLastPos.set(
                        planetInstancePosition.x,
                        planetInstancePosition.y,
                        planetInstancePosition.z
                    );
                    siteList[sitenumber].firstPosFlag = false;
                    scene.remove(planets[i]);
                    planets[i].remove(labelsPivots[i]);
                    labelsPivots[i] = null
                    planets[i] = null;
                    planets[i] = createPlanet(planetInstancePosition,siteList[sitenumber].siteName,i);
                }
            }else{
                if(!siteList[sitenumber].firstPosFlag){
                    siteList[sitenumber].planetFirstPos.set(
                        planetInstancePosition.x,
                        planetInstancePosition.y,
                        planetInstancePosition.z
                    );
                    siteList[sitenumber].firstPosFlag = true;
                    scene.remove(planets[i]);
                    planets[i].remove(labelsPivots[i]);
                    labelsPivots[i] = null
                    planets[i] = null;
                    planets[i] = createPlanet(planetInstancePosition,siteList[getRandomInt(0,siteList.length-1)].siteName,i);
                }else if(siteList[sitenumber].firstPosFlag){
                    siteList[sitenumber].planetLastPos.set(
                        planetInstancePosition.x,
                        planetInstancePosition.y,
                        planetInstancePosition.z
                    );
                    siteList[sitenumber].firstPosFlag = false;
                    scene.remove(planets[i]);
                    planets[i].remove(labelsPivots[i]);
                    labelsPivots[i] = null
                    planets[i] = null;
                    planets[i] = createPlanet(planetInstancePosition,siteList[getRandomInt(0,siteList.length-1)].siteName,i);
                }
            }
        }else{
            customNumber = getRandomInt(0,siteList.length-1);
            if(customNumber < maxListNumber && (siteList[customNumber] != null || siteList[customNumber] != undefined )){
                if(!siteList[customNumber].firstPosFlag){
                    siteList[customNumber].planetFirstPos.set(
                        planetInstancePosition.x,
                        planetInstancePosition.y,
                        planetInstancePosition.z
                    );
                    siteList[customNumber].firstPosFlag = true;
                    scene.remove(planets[i]);
                    planets[i].remove(labelsPivots[i]);
                    labelsPivots[i] = null
                    planets[i] = null;
                    planets[i] = createPlanet(planetInstancePosition,siteList[customNumber].siteName,i);
                }else if(siteList[customNumber].firstPosFlag){
                    siteList[customNumber].planetLastPos.set(
                        planetInstancePosition.x,
                        planetInstancePosition.y,
                        planetInstancePosition.z
                    );
                    siteList[customNumber].firstPosFlag = false;
                    scene.remove(planets[i]);
                    planets[i].remove(labelsPivots[i]);
                    labelsPivots[i] = null
                    planets[i] = null;
                    planets[i] = createPlanet(planetInstancePosition,siteList[customNumber].siteName,i);
                }
            }else{
                if(!siteList[customNumber].firstPosFlag){
                    siteList[customNumber].planetFirstPos.set(
                        planetInstancePosition.x,
                        planetInstancePosition.y,
                        planetInstancePosition.z
                    );
                    siteList[customNumber].firstPosFlag = true;
                    scene.remove(planets[i]);
                    planets[i].remove(labelsPivots[i]);
                    labelsPivots[i] = null
                    planets[i] = null;
                    planets[i] = createPlanet(planetInstancePosition,siteList[getRandomInt(0,siteList.length-1)].siteName,i);
                }else if(siteList[customNumber].firstPosFlag){
                    siteList[customNumber].planetLastPos.set(
                        planetInstancePosition.x,
                        planetInstancePosition.y,
                        planetInstancePosition.z
                    );
                    siteList[customNumber].firstPosFlag = false;
                    scene.remove(planets[i]);
                    planets[i].remove(labelsPivots[i]);
                    labelsPivots[i] = null
                    planets[i] = null;
                    planets[i] = createPlanet(planetInstancePosition,siteList[getRandomInt(0,siteList.length-1)].siteName,i);
                }
            }
        }
    }
    shouldRedoRender = false;
    animate();
}

function placeCrossHair(){
    const crossHairElement = document.getElementById("crosshair");
    const sizeCrossHair = parseFloat( getComputedStyle(crossHairElement).getPropertyValue("--sizeCrossHair"));
    crossHairElement.style.setProperty("--centerPositionY", ((window.innerHeight - sizeCrossHair)/2).toString()+"px");
    crossHairElement.style.setProperty("--centerPositionX", ((window.innerWidth - sizeCrossHair)/2).toString()+"px");
}

function setCategoriesList(){
    const categoriesButton = document.getElementById("CategoriesListToggle");
    const iconElem = document.getElementById("icon");
    const sizeFullScreenButton = parseFloat( getComputedStyle(categoriesButton).getPropertyValue("width"));
    const paddingFullScreenButton = parseFloat( getComputedStyle(iconElem).getPropertyValue("top"));
    const padnumber = parseFloat( getComputedStyle(categoriesButton).getPropertyValue("top"));
    
    categoriesButton.style.top = (sizeFullScreenButton + paddingFullScreenButton + padnumber).toString()+'px';
    const categoriesList = document.getElementById("CategoriesList");
    var listElement = document.getElementById("CategoriesListToggle");
    categoriesList.style.top = (-sizeFullScreenButton/2).toString()+'px';
    // categoriesList.style.left = (parseInt( getComputedStyle(categoriesList).getPropertyValue("--minCategoriesListWidth"))*0.5)*-1;
    categoriesButton.onclick = (e) => {
        document.getElementById("CategoriesList").classList.toggle("show");
    }
    const controlsHintElement = document.getElementById("controlsToggle");
    controlsHintElement.onclick = (e) =>{
        document.getElementById("controlsContainer").classList.toggle("showControls");
    }
    var categoriesNames = categoriesList.children;
    for(var i = 0; i < categoriesNames.length; i++){
        if(categoriesNames[i]){
            categoriesNames[i].onclick = (e) =>{        
                awesomeCategory = e.target.getAttribute("data-name");
                console.log(e.target.getAttribute("data-name"));
                redoRenderHelper(awesomeCategory);
            }
        }
    }
    document.addEventListener("keydown",colorPressing,false);
    document.addEventListener("keyup",colorUnPressing,false);
    
}

function colorPressing(event){
    const controlsHintElement = document.getElementById("controlsContainer");
    var controlsItems = controlsHintElement.children;
    var toToggle = null;
    for(var i = 0; i < controlsItems.length; i++){
        if(controlsItems[i].children[0]){
            const toggleCandidate =controlsItems[i];
            if(event.key){
                // === controlsItems[i].children[0].children[0].textContent.toLowerCase()
                if(event.key.toLowerCase() === controlsItems[i].children[0].getAttribute("x-controlKey").toLowerCase()){
                    if(!toggleCandidate.classList.contains("coloredControlKey")){
                        toggleCandidate.classList.add("coloredControlKey");
                    }
                    //
                }
                //
            }
        }
    }
}

function colorUnPressing(event){
    // document.getElementById("controlsContainer").children[0].children[0].children[0].textContent.toLowerCase();
    const controlsHintElement = document.getElementById("controlsContainer");
    var controlsItems = controlsHintElement.children;
    for(var i = 0; i < controlsItems.length; i++){
        if(controlsItems[i].children[0]){
            const toggleCandidate =controlsItems[i];
            if(event.key){
                // === controlsItems[i].children[0].children[0].textContent.toLowerCase()
                if(event.key.toLowerCase() === controlsItems[i].children[0].getAttribute("x-controlKey").toLowerCase()){
                    if(toggleCandidate.classList.contains("coloredControlKey")){
                        toggleCandidate.classList.remove("coloredControlKey");
                    }
                    //
                }
                //
            }
        }
    }
}

document.addEventListener("mousedown",function(event) {
    const leftClick = event.buttons & 1;
    const rightClick = event.buttons & 2;
    const controlsHintElement = document.getElementById("controlsContainer");
    var controlsItems = controlsHintElement.children;
    for(var i = 0; i < controlsItems.length; i++){
        if(controlsItems[i].children[0]){
            const toggleCandidate =controlsItems[i];
            // === controlsItems[i].children[0].children[0].textContent.toLowerCase()
            if(controlsItems[i].children[0].getAttribute("x-controlKey").toLowerCase() ==="leftclick" && leftClick === 1){
                if(!toggleCandidate.classList.contains("coloredControlKey")){
                    toggleCandidate.classList.add("coloredControlKey");
                }
                //
            }
            if(controlsItems[i].children[0].getAttribute("x-controlKey").toLowerCase() ==="rightclick" && rightClick === 2){
                if(!toggleCandidate.classList.contains("coloredControlKey")){
                    toggleCandidate.classList.add("coloredControlKey");
                }
                //
            }
        }
    }
},false);
document.addEventListener("mouseup",function(event) {
    const leftClick = event.buttons & 1;
    const rightClick = event.buttons & 2;
    const controlsHintElement = document.getElementById("controlsContainer");
    var controlsItems = controlsHintElement.children;
    for(var i = 0; i < controlsItems.length; i++){
        if(controlsItems[i].children[0]){
            const toggleCandidate =controlsItems[i];
            // === controlsItems[i].children[0].children[0].textContent.toLowerCase()
            if(controlsItems[i].children[0].getAttribute("x-controlKey").toLowerCase() ==="leftclick" && leftClick === 0){
                if(toggleCandidate.classList.contains("coloredControlKey")){
                    toggleCandidate.classList.remove("coloredControlKey");
                }
                //
            }
            if(controlsItems[i].children[0].getAttribute("x-controlKey").toLowerCase() ==="rightclick" && rightClick === 0){
                if(toggleCandidate.classList.contains("coloredControlKey")){
                    toggleCandidate.classList.remove("coloredControlKey");
                }
                //
            }
        }
    }
},false);

function colorUnpressingBlur(){
    const controlsHintElement = document.getElementById("controlsContainer");
    var controlsItems = controlsHintElement.children;
    for(var i = 0; i < controlsItems.length; i++){
        if(controlsItems[i].children[0]){
            const toggleCandidate =controlsItems[i];
            if(toggleCandidate.classList.contains("coloredControlKey")){
                toggleCandidate.classList.remove("coloredControlKey");
            }
        }
    }
    //
}

